package com.wiyatmoko.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wiyatmoko.data.Image
import com.wiyatmoko.databinding.ImageSliderItemsBinding
import com.wiyatmoko.untils.ImageLoader

class ImageAdapter(
private val imageList: List<Image>
): RecyclerView.Adapter<ImageAdapter.ImageVH>() {

    class ImageVH(private val binding: ImageSliderItemsBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(image: Image) {
            ImageLoader.loadImage(image.url, binding.imageContent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageVH {
        val binding = ImageSliderItemsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ImageVH(binding)
    }

    override fun onBindViewHolder(holder: ImageVH, position: Int) {
        holder.bind(imageList[position])
    }

    override fun getItemCount(): Int {
        return imageList.size
    }
}