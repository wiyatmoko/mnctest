package com.wiyatmoko.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.wiyatmoko.R
import com.wiyatmoko.data.ArticleResponse
import com.wiyatmoko.databinding.ItemArticleBinding
import javax.inject.Inject

class ArticleAdapter @Inject constructor(
    private val contentList: List<ArticleResponse>
) : RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {
    private var onItemClickListener: OnItemClickListener? = null
    private val datas = contentList
    interface OnItemClickListener {
        fun onItemClick(data: ArticleResponse)
    }

    inner class ViewHolder(val binding: ItemArticleBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val viewBinding  =
           ItemArticleBinding.inflate( LayoutInflater.from(viewGroup.context), viewGroup, false)
        return ViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = datas[position]
        val imageData = contentList[position].contentThumbnail
        val glideOpt =
            RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).fitCenter()
                .placeholder(R.drawable.ic_launcher_background)
        Glide.with(holder.binding.thumbnailImage.context)
            .load("$imageData")
            .apply(glideOpt)
            .thumbnail(0.1f)
            .into(holder.binding.thumbnailImage)
        holder.binding.contributorName.text = contentList[position].contributorName
        holder.binding.title.text = contentList[position].title
        holder.itemView.setOnClickListener {
            onItemClickListener?.onItemClick(data)
        }

    }

    override fun getItemCount(): Int {
        return contentList.size
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        onItemClickListener = listener
    }


}