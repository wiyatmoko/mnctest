package com.wiyatmoko

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp()
class Wiyatmoko: Application(){
}