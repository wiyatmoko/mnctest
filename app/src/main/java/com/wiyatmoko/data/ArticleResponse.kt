package com.wiyatmoko.data

import com.google.gson.annotations.SerializedName


data class ArticleResponse(
    @SerializedName("content")
    val content: String,
    @SerializedName("contentThumbnail")
    val contentThumbnail: String,
    @SerializedName("contributorAvatar")
    val contributorAvatar: String,
    @SerializedName("contributorName")
    val contributorName: String,
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("slideshow")
    val slideshow: List<String>,
    @SerializedName("title")
    val title: String
)