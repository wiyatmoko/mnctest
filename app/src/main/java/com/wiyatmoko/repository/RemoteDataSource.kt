package com.wiyatmoko.repository

import com.wiyatmoko.configuration.api.ApiInterfaceArticle
import com.wiyatmoko.data.ArticleResponse
import com.wiyatmoko.data.ResultArticle
import retrofit2.Response
import javax.inject.Inject

class RemoteDataSource @Inject constructor  (
    private val apiInterfaceArticle: ApiInterfaceArticle
){
    suspend fun getArticle(): Response<ResultArticle> {
        return apiInterfaceArticle.getArticle()
    }

    suspend fun getDetailArticle(id: Int): Response<ArticleResponse> {
        return apiInterfaceArticle.getDetailArticle(id)
    }
}

