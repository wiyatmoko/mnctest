package com.wiyatmoko.component.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.wiyatmoko.adapter.ArticleAdapter
import com.wiyatmoko.data.ArticleResponse
import com.wiyatmoko.data.NetworkResult
import com.wiyatmoko.data.ResultArticle
import com.wiyatmoko.databinding.FragmentHomeBinding
import com.wiyatmoko.viewModel.ArticleViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private val articleViewModel: ArticleViewModel by viewModels()
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var mSwipeRefreshLayout: SwipeRefreshLayout
    private lateinit var articleAdapter: ArticleAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        mSwipeRefreshLayout = binding.swipeRefreshLayout
        mSwipeRefreshLayout.setOnRefreshListener {
            observeViewModel()
        }
        loadDataArticle()
        lifecycleScope.launch(){
            articleViewModel.getArticle()
            observeViewModel()
        }



        return root

    }

    private fun loadDataArticle() {
        lifecycleScope.launch {
            mSwipeRefreshLayout.isRefreshing = false
            observeViewModel()
        }

    }


    private fun observeViewModel() {
        articleViewModel.getArticle()
        articleViewModel._articleResponse.observe(viewLifecycleOwner) {
            loadData(it)
        }

    }

    private fun loadData(response: NetworkResult<ResultArticle>) {
        when (response) {
            is NetworkResult.Success -> {
                mSwipeRefreshLayout.isRefreshing = false
                response.data?.let { postValue(it) }
            }
            is NetworkResult.Error -> {
                Toast.makeText(requireContext(), response.message.toString(), Toast.LENGTH_SHORT)
                    .show()
            }
            is NetworkResult.Loading -> {
            }
        }

    }

    private fun postValue(data: List<ArticleResponse>) {
        if (data != null) {
            val recyclerView: RecyclerView = binding.rlArticle
            linearLayoutManager = LinearLayoutManager(requireContext())
            recyclerView.layoutManager = linearLayoutManager
            articleAdapter = ArticleAdapter(data)
            recyclerView.adapter = articleAdapter
            recyclerView.setHasFixedSize(true)

            articleAdapter.setOnItemClickListener(object : ArticleAdapter.OnItemClickListener{
                override fun onItemClick(data: ArticleResponse) {
                    val direction = HomeFragmentDirections.actionNavigationHomeToDetailFragment(data.id.toInt())
                    findNavController().navigate(direction)
                }
            })

        }
    }

}