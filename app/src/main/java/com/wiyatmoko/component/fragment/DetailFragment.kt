package com.wiyatmoko.component.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.ViewPager2
import com.wiyatmoko.adapter.ImageAdapter
import com.wiyatmoko.adapter.ThumbnailAdapter
import com.wiyatmoko.data.ArticleResponse
import com.wiyatmoko.data.Image
import com.wiyatmoko.databinding.FragmentDetailBinding
import com.wiyatmoko.untils.CommonService
import com.wiyatmoko.viewModel.ArticleViewModelDetail
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.util.Date

@AndroidEntryPoint
class DetailFragment : Fragment() {
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val args : DetailFragmentArgs by navArgs()

    private lateinit var imageAdapter: ImageAdapter
    private lateinit var thumbnailAdapter: ThumbnailAdapter

    private val articleViewModelDetail: ArticleViewModelDetail by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        val root: View = binding.root

        lifecycleScope.launch(){
            articleViewModelDetail.getDetailArticle(args.id)
            observeViewModel()
        }
        return root
    }

    private fun observeViewModel() {
        articleViewModelDetail
        articleViewModelDetail._articleResponse.observe(viewLifecycleOwner) {
            loadData(it)
        }

    }

    private fun loadData(data: ArticleResponse) {
        val date : Date? = CommonService.getConvertDate(data.createdAt)
        binding.dateContent.text = date.toString()
        binding.contributorNameDetail.text = data.contributorName
        binding.titleDetail.text = data.title
        binding.descriptionContent.text = data.content
        var imageList = mutableListOf<Image>()
        if(data.slideshow.isNotEmpty()){
            for (value in data.slideshow) {
                imageList.add(Image(value))
            }

        } else {
            imageList.add(Image(data.contentThumbnail))
        }

        imageAdapter = ImageAdapter(imageList)
        binding.vpImage.adapter = imageAdapter

        thumbnailAdapter = ThumbnailAdapter(imageList) {
            // when click on thumbnail, viewpager should scroll to corresponding position
            binding.vpImage.currentItem = imageList.indexOf(it)
        }
        binding.rvThumbnail.adapter = thumbnailAdapter
        binding.rvThumbnail.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        binding.vpImage.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                // update thumbnail border color
                thumbnailAdapter.updateSelectedPosition(position)
                // smooth scroll to corresponding position when thumbnail is outside the view
                binding.rvThumbnail.smoothScrollToPosition(position)
            }
        })


    }


}