package com.wiyatmoko.configuration.api

import com.wiyatmoko.data.ArticleResponse
import com.wiyatmoko.data.ResultArticle
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterfaceArticle {
    @GET("/api/innocent/newsapp/articles")
    suspend fun getArticle(): Response<ResultArticle>

    @GET("/api/innocent/newsapp/articles/{id}")
    suspend fun getDetailArticle(@Path("id") id : Int): Response<ArticleResponse>
}