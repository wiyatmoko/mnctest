package com.wiyatmoko.untils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.wiyatmoko.R


object ImageLoader {

    @JvmStatic
    @JvmOverloads
    fun loadImage(url: String, imageView: ImageView,
                  placeholder: Int = R.drawable.banner_placeholder,
                  error: Int = R.drawable.banner_placeholder) {
        val glideOpt =
            RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).fitCenter()
                .placeholder(placeholder)
        Glide.with(imageView.context)
            .load(url)
            .apply(glideOpt)
            .error(error)
            .thumbnail(0.1f)
            .into(imageView)
    }
}