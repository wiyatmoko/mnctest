package com.wiyatmoko.untils

import android.util.Log.v
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.TimeZone

object CommonService {


    fun getConvertDate(strDate: String): Date? {
        return strDate?.getDateWithServerTimeStamp()
    }

    fun String.getDateWithServerTimeStamp(): Date? {
        val dateFormat = SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            Locale.getDefault()
        )
        dateFormat.timeZone = TimeZone.getDefault()
        try {
            return dateFormat.parse(this)
        } catch (e: ParseException) {
            return null
        }
    }
}