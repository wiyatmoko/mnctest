package com.wiyatmoko.untils

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.ISODateTimeFormat
import java.lang.reflect.Type

class DateTimeConverters : JsonSerializer<DateTime>, JsonDeserializer<DateTime?> {

    override fun serialize(src: DateTime, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
        val dateTimeNoMillisFmt = ISODateTimeFormat.dateTimeNoMillis()
        return JsonPrimitive(dateTimeNoMillisFmt.print(src))
    }

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): org.joda.time.DateTime? {
        return convertStringToDate(json.asString)
    }

    companion object {
        private val DATE_FORMATS = arrayOf("yyyy-MM-dd'T'HH:mm:ss.SSSZ",
            "yyyy-MM-dd HH:mm:ss",
            "yyyy-MM-dd")

        fun convertStringToDate(value: String?): org.joda.time.DateTime? {
            for(format in DATE_FORMATS) {
                try {
                    return if (value?.toIntOrNull() != null){
                        org.joda.time.DateTime(value.toLong() * 1000, DateTimeZone.getDefault())
                    }else {
                        val formatter = DateTimeFormat.forPattern(format)
                        formatter.parseDateTime(value)
                    }
                } catch(e: IllegalArgumentException) {
                    continue
                }
            }
            return null
        }
    }
}