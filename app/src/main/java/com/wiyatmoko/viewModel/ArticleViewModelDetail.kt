package com.wiyatmoko.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wiyatmoko.data.ArticleResponse
import com.wiyatmoko.data.NetworkResult
import com.wiyatmoko.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ArticleViewModelDetail @Inject constructor(
    private val repository: Repository
) : ViewModel() {

    var _articleResponse: MutableLiveData<ArticleResponse> = MutableLiveData()

    fun getDetailArticle(id: Int) = viewModelScope.launch {
        val response = repository.remote.getDetailArticle(id)
        _articleResponse.value = response.body()
    }
}