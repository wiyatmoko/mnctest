package com.wiyatmoko.viewModel


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wiyatmoko.data.NetworkResult
import com.wiyatmoko.data.ResultArticle
import com.wiyatmoko.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ArticleViewModel @Inject constructor(
    private val repository: Repository
) : ViewModel() {

    var _articleResponse: MutableLiveData<NetworkResult<ResultArticle>> = MutableLiveData()


    fun getArticle() = viewModelScope.launch {
        _articleResponse.value = NetworkResult.Loading()
        val response = repository.remote.getArticle()
        _articleResponse.value = handleResponseGetArticle(response)


    }

    private fun handleResponseGetArticle(response: Response<ResultArticle>): NetworkResult<ResultArticle>? {

        return when {
            response.message().toString().contains("timeout") -> {
                NetworkResult.Error("Timeout")
            }
            response.code() == 401 -> {
                NetworkResult.Error("Access denied")
            }
            response.isSuccessful -> {
                val articleResponse = response.body()
                NetworkResult.Success(articleResponse!!)
            }
            else -> {
                NetworkResult.Error(response.message())
            }
        }

    }


}